<?php

namespace App\Repository\Article;

use App\Entity\Articulos\ArticlesReferences;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArticlesReferemces|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticlesReferemces|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticlesReferemces[]    findAll()
 * @method ArticlesReferemces[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlesReferencesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticlesReferences::class);
    }

    // /**
    //  * @return ArticlesReferemces[] Returns an array of ArticlesReferemces objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArticlesReferemces
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
