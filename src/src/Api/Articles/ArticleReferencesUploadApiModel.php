<?php

declare(strict_types=1);

namespace App\Api\Articles;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
class ArticleReferencesUploadApiModel
{


    /**
     *
     * @Assert\NotBlank()
     */
    public $filename;

    /**
     *
     * @Assert\NotBlank()
     */
    private $data;

    private $decoderData;

    /**
     * Get the value of data.
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data.
     *
     * @return self
     */
    public function setData(?string $data)
    {
        $this->data = $data;
        $this->decoderData = base64_decode($data);
    }

    public function getDecoderDate() : ?string
    {
        return $this->decoderData;
    }
}
