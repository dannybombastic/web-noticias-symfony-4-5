<?php

namespace App\Entity\Comment;

use App\Entity\Articulos\Articles;
use App\Entity\User\UserProfile;
use App\Repository\Comment\CommentsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comments.
 *
 * @ORM\Table(name="comments", indexes={@ORM\Index(name="id_user", columns={"id_user"}), @ORM\Index(name="id_art", columns={"id_art"})})
 * @ORM\Entity
 */

/**
 * @ORM\Entity(repositoryClass=CommentsRepository::class)
 */
class Comments
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="art_comment", type="text",  nullable=false)
     */
    private $artComment;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive = false;

    /**
     * @var UserProfile
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\user\UserProfile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\ManyToOne(targetEntity=Articles::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorname;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArtComment(): ?string
    {
        return $this->artComment;
    }

    public function setArtComment(string $artComment): self
    {
        $this->artComment = $artComment;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIdUser(): ?UserProfile
    {
        return $this->idUser;
    }

    public function setIdUser(?UserProfile $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getArticle(): ?Articles
    {
        return $this->article;
    }

    public function setArticle(?Articles $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getAuthorname(): ?string
    {
        return $this->authorname;
    }

    public function setAuthorname(?string $authorname): self
    {
        $this->authorname = $authorname;

        return $this;
    }
}
