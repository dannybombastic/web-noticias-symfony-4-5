<?php

namespace App\Entity\Articulos;

use App\Repository\Article\CategorieRepository;
use App\Service\UploaderService\UpLoadHelper;
use Doctrine\ORM\Id\UuidGenerator;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Categories.
 *
 * @ORM\Table(name="categories")
 * @ORM\Entity
 */

/**
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categories
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     * @ORM\Column(name="id", type="uuid", length=40, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=220, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=220, nullable=false)
     */
    private $image;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    public function getImageUrl()
    {
        return UpLoadHelper::CATEGORY_IMAGE.'/'.$this->getImage();
    }

    public function getFileName(): string
    {
        return $this->getImage();
    }

    public function getFileNameWithFolder(): string
    {
        return UpLoadHelper::CATEGORY_IMAGE.'/'.$this->getImage();
    }
}
