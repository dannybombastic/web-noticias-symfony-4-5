<?php

namespace App\Entity\Articulos;

use App\Entity\Comment\Comments;
use App\Entity\Tags\Tageed;
use App\Entity\User\UserProfile;
use App\Repository\Article\ArticlesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Articles.
 *
 * @ORM\Table(name="articles", indexes={@ORM\Index(name="id_cat", columns={"id_cat"})})
 * @ORM\Entity
 */

/**
 * @ORM\Entity(repositoryClass=ArticlesRepository::class)
 */
class Articles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="art_title", type="string", length=220, nullable=false)
     * @Assert\NotBlank(message="Get creative and think of a title")
     */
    private $artTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="art_desc", type="string", length=400, nullable=false)
     */
    private $artDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="art_evalu", type="integer",  nullable=false, options={"fixed"=true})
     */
    private $artEvalu;

    /**
     * @var int
     *
     * @ORM\Column(name="visited_cnt", type="integer", nullable=false)
     */
    private $visitedCnt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="visited_at", type="datetime", nullable=false)
     */
    private $visitedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var Categories
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Articulos\Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cat", referencedColumnName="id")
     * })
     */
    private $idCat = null;

    /**
     * @ORM\ManyToOne(targetEntity=UserProfile::class, inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $heartCount;

    /**
     * @ORM\OneToMany(targetEntity=Comments::class, mappedBy="article", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"createAt" = "DESC"})
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Tageed::class, inversedBy="articles")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity=ArticlesReferences::class, mappedBy="article")
     * @ORM\OrderBy({"position"="ASC"})
     */
    private $articlesReferences;

    /**
     * @Column(type="geometry", options={"geometry_type"="POINT", "srid"=3785}, nullable=true)
     */
    private $point;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->articlesReferemces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArtTitle(): ?string
    {
        return $this->artTitle;
    }

    public function setArtTitle(string $artTitle): self
    {
        $this->artTitle = $artTitle;

        return $this;
    }

    public function getArtDesc(): ?string
    {
        return $this->artDesc;
    }

    public function setArtDesc(string $artDesc): self
    {
        $this->artDesc = $artDesc;

        return $this;
    }

    public function getArtEvalu(): ?string
    {
        return $this->artEvalu;
    }

    public function setArtEvalu(string $artEvalu): self
    {
        $this->artEvalu = $artEvalu;

        return $this;
    }

    public function getVisitedCnt(): ?int
    {
        return $this->visitedCnt;
    }

    public function setVisitedCnt(int $visitedCnt): self
    {
        $this->visitedCnt = $visitedCnt;

        return $this;
    }

    public function getVisitedAt(): ?\DateTimeInterface
    {
        return $this->visitedAt;
    }

    public function setVisitedAt(\DateTimeInterface $visitedAt): self
    {
        $this->visitedAt = $visitedAt;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIdCat(): ?Categories
    {
        return $this->idCat;
    }

    public function setIdCat(?Categories $idCat): self
    {
        $this->idCat = $idCat;

        return $this;
    }

    public function getAuthor(): ?UserProfile
    {
        return $this->author;
    }

    public function setAuthor(?UserProfile $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getHeartCount(): ?int
    {
        return $this->heartCount;
    }

    public function setHeartCount(?int $heartCount): self
    {
        $this->heartCount = $heartCount;

        return $this;
    }

    public function incrementHerts(): self
    {
        $this->setHeartCount($this->getHeartCount() + 1);

        return $this;
    }

    /**
     * @return Collection|Comments[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * @return Collection|Comments[]
     */
    public function getNotDletedComments(): Collection
    {
        $criteria = ArticlesRepository::findIsActive();

        return $this->comments->matching($criteria);
    }

    public function addComment(Comments $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tageed[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tageed $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tageed $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getArtTitle();
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (false !== stripos($this->getArtTitle(), 'porras')) {
            $context->buildViolation('The name must not contains porras')
                ->atPath('title')
                ->addViolation();
        }
    }

    /**
     * @return Collection|ArticlesReferences[]
     */
    public function getarticlesReferences(): Collection
    {
        return $this->articlesReferences;
    }

    /**
     * Get the value of point.
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set the value of point.
     *
     * @return self
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }
}
