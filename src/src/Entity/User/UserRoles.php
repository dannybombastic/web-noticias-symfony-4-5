<?php

namespace App\Entity\User;

use App\Repository\UserRolesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserRoles.
 *
 * @ORM\Table(name="user_roles")
 * @ORM\Entity
 */

/**
 * @ORM\Entity(repositoryClass=UserRolesRepository::class)
 */
class UserRoles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=UserProfile::class, inversedBy="userRoles", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = ['ROLE_USER'];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?UserProfile
    {
        return $this->user;
    }

    public function setUser(UserProfile $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function __toString()
    {
        return $this->user->getUserName();
    }
}
