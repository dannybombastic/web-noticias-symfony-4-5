<?php

namespace App\Entity\User;

use App\Entity\Articulos\Articles;
use App\Repository\User\UserProfileRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserProfile.
 *
 * @ORM\Table(name="user_profile", indexes={@ORM\Index(name="user_role", columns={"user_role"})})
 * @ORM\Entity
 */
/**
 * @ORM\Entity(repositoryClass=UserProfileRepository::class)
 * @UniqueEntity(
 *      fields={"id"},
 *      message="I think you have already regitered!"
 * )
 */
class UserProfile implements UserInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=24, nullable=false, unique=true)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=240, nullable=false)
     */
    private $userName;

    /**
     * @var string
     * @Groups("main")     *
     * @ORM\Column(name="user_addr", type="string", length=60, nullable=false)
     */
    private $userAddr;

    /**
     * @var string
     * @Groups("main")
     * @ORM\Column(name="user_email", type="string", length=240, nullable=false)
     * @Assert\NotBlank(message="Please Enter An email")
     * @Assert\Email()
     */
    private $Email;

    /**
     * @var string
     *
     * @ORM\Column(name="user_password", type="string", length=150, nullable=false)
     */
    private $password;

    /**
     * @var string
     * @Groups("main")
     * @ORM\Column(name="gender", type="string", length=255, nullable=false)
     */
    private $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @var bool
     * @Groups("main")
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @Groups("main")
     * @ORM\Column(name="roles", type="simple_array", nullable=false)
     */
    private $roles;

    /**
     * @Groups("main")
     * @ORM\OneToMany(targetEntity=\App\Entity\User\ApiToken::class, mappedBy="user", orphanRemoval=true )
     */
    private $apiTokens;

    /**
     * @ORM\OneToMany(targetEntity=Articles::class, mappedBy="author")
     */
    private $articles;

    /**
     * @ORM\Column(type="datetime")
     */
    private $aggreTermsAt;

    /**
     * @ORM\OneToOne(targetEntity=UserRoles::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $userRoles;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function __construct()
    {
        $this->apiTokens = new ArrayCollection();
        $this->createAt = new DateTime();
        $this->articles = new ArrayCollection();
        $this->userRoles = new UserRoles();
        $this->userRoles->setUser($this);
        $this->roles = $this->getRoles();
        $this->isActive = 1;
    }

    public function getAvatarUrl(int $size = null): string
    {
        $url = 'https://robohash.org/'.$this->getUserName();

        if ($size) {
            $url .= sprintf('?size=%dx%d', $size, $size);
        }

        return $url;
    }

    public function getuserDni(): ?string
    {
        return $this->id;
    }

    public function setuse(string $userDni): ?string
    {
        $this->id = $userDni;

        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    public function setUserName(string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    public function getUserAddr(): ?string
    {
        return $this->userAddr;
    }

    public function setUserAddr(string $userAddr): self
    {
        $this->userAddr = $userAddr;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
    }

    public function getuserEmail(): ?string
    {
        return $this->Email;
    }

    public function setuserEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getRoles(): ?array
    {
        $roles = $this->getUserRoles()->getRoles();

        return array_unique($roles);
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Set the value of roles.
     *
     * @return self
     */
    public function setRoles(array $roles)
    {
        $this->userRoles->setRoles($roles);

        $this->roles = $this->userRoles->getRoles();

        return $this;
    }

    /**
     * @return Collection|\App\Entity\ApiToken[]
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }



    /**
     * @return Collection|Articles[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Articles $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setAuthor($this);
        }

        return $this;
    }

    public function removeArticle(Articles $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getAuthor() === $this) {
                $article->setAuthor(null);
            }
        }

        return $this;
    }

    public function getAggreTermsAt(): bool
    {
        return null === $this->aggreTermsAt;
    }

    public function setAggreTermsAt(bool $agree): self
    {
        $this->aggreTermsAt = $agree ? new DateTime() : null;

        return $this;
    }

    public function agreeterms()
    {
        $this->setAggreTermsAt($this->getAggreTermsAt());
    }

    public function getUserRoles(): ?UserRoles
    {
        return $this->userRoles;
    }

    public function setUserRoles(UserRoles $userRoles): self
    {
        $this->userRoles = $userRoles;

        // set the owning side of the relation if necessary
        if ($userRoles->getUser() !== $this) {
            $userRoles->setUser($this);
        }

        return $this;
    }
}
