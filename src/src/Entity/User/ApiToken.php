<?php

namespace App\Entity\User;

use App\Repository\User\ApiTokenRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApiTokenRepository::class)
 */
class ApiToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expriteAt;



    /**
     * @ORM\ManyToOne(targetEntity=UserProfile::class, inversedBy="apiTokens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct(UserProfile $user)
    {
        $this->token = bin2hex(random_bytes(60));
        $this->user = $user;
        $this->expriteAt = new DateTime('+1 hour');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getExpriteAt(): ?\DateTimeInterface
    {
        return $this->expriteAt;
    }

    public function getUser(): ?UserProfile
    {
        return $this->user;
    }

    public function isExpired()
    {       
        return $this->getExpriteAt() <= new DateTime();
    }

    public function setToken(UserProfile $user)
    {
        $this->token = bin2hex(random_bytes(60));
        $this->user = $user;
        $this->expriteAt = new DateTime('+5 hour');
    }

    /**
     * @param DateTime $expriteAt
     */
    public function setExpriteAt(DateTime $expriteAt): void
    {
        $this->expriteAt = $expriteAt;
    }

    public function addTime()
    {
        $this->setExpriteAt( new DateTime('+5 hour'));
    }
}
