<?php

namespace App\Controller\Articulos;

use App\Controller\BaseController;
use App\Entity\Articulos\Categories;
use App\Form\Articulos\CategoriesType;
use App\Repository\Article\CategorieRepository;
use App\Service\UploaderService\UpLoadHelper;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/articulos/categories")
 * @IsGranted("ROLE_ADMIN")
 */
class CategoriesController extends BaseController
{
    /**
     * @Route("/admin", name="articulos_categories_index")
     */
    public function index(CategorieRepository $categorieRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $q = $request->query->get('q');
        $queryBuilder = $categorieRepository->getWithShareQuerybuilder($q);

        $paginator = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            10 /*limit per page*/
        );

        return $this->render('articulos/categories/index.html.twig', [
            'pagination' => $paginator,
        ]);
    }

    /**
     * @Route("/new", name="articulos_categories_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValidatorInterface $validator, UpLoadHelper $uploadHelper): Response
    {
        $category = new Categories();
        $form = $this->createForm(CategoriesType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newsFile = $form['image']->getData();
            $violations = $validator->validate(
                $newsFile,
                [
                    new NotBlank([
                        'message' => 'Hijo de la verga sube un archivo canijo',
                    ]),
                    new File([
                        'maxSize' => '1k',
                        'mimeTypes' => [
                            'image/*',
                            'application/pdf',
                        ],
                    ]),
                ]
            );
            if ($violations->count() > 0) {
                /** @var ConstraintViolation $violations */
                $violations = $violations[0];
                //  $this->addFlash('error', $violations->getMessage());
                // return $this->redirectToRoute(route_name);
            }

            if ($newsFile) {
                $newFileName = $uploadHelper->uploadCategorieImage($newsFile, null);
                //$newFileName = $fileUploader->upload($newsFile);
                $category->setImage($newFileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('articulos_categories_index');
        }

        return $this->render('articulos/categories/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    // /**
    //  * @Route("/admin/upload" , name="app_upload")
    //  */
    // public function temporaryupload(Request $request)
    // {
    //     /** @var UploadedFile $uploadFile */
    //     $uploadFile = $request->files->get('image');
    //     $destination = $this->getParameter('kernel.project_dir') . '/assets/uploads';

    //     $originalNmae = pathinfo($uploadFile->getClientOriginalName(), PATHINFO_FILENAME);
    //     $newFilename = Urlizer::urlize($originalNmae) . uniqid() . '-' . $uploadFile->guessExtension();
    //     $uploadFile->move(
    //         $destination,
    //         $newFilename
    //     );

    //     return new Response($destination, 200, []);
    // }

    /**
     * @Route("/{id}", name="articulos_categories_show", methods={"GET"})
     */
    public function show(Categories $category): Response
    {
        return $this->render('articulos/categories/show.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="articulos_categories_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Categories $category, UpLoadHelper $uploadHelper): Response
    {
        $form = $this->createForm(CategoriesType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newsFile = $form['image']->getData();
            if ($newsFile) {
                $newFileName = $uploadHelper->uploadCategorieImage($newsFile, $category->getFileNameWithFolder());
                $category->setImage($newFileName);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('articulos_categories_index');
        }

        return $this->render('articulos/categories/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="articulos_categories_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Categories $category): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('articulos_categories_index');
    }
}
