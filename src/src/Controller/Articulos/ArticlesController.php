<?php

namespace App\Controller\Articulos;

use App\Controller\BaseController;
use App\Entity\Articulos\Articles;
use App\Entity\Articulos\Categories;
use App\Entity\Articulos\Multimedia;
use App\Form\Articulos\ArticlesType;
use App\Repository\Article\ArticlesRepository;
use App\Service\Markdown\MarkdownHelper;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/articulos/articles")
 * @IsGranted("ROLE_ADMIN")
 */
class ArticlesController extends BaseController
{
    /**
     * @Route("/", name="articulos_articles_index")
     */
    public function index(ArticlesRepository $articlesRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $q = $request->query->get('q');

        $queryBuilder = $articlesRepository->getWithShareQuerybuilder($q);

        $paginator = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            10 /*limit per page*/
        );

        return $this->render('articulos/articles/index.html.twig', [
            'pagination' => $paginator,
        ]);
    }

    /**
     * @Route("/new", name="articulos_articles_new", methods={"GET","POST"})
     */
    public function new(Request $request, MarkdownHelper $markdownHelper): Response
    {
        //$markdownHelper->parse("**holaaaaa cat**");
        $categorias = $this->getDoctrine()->getRepository(Categories::class)->findAll();
        $article = new Articles();
        $form = $this->createForm(ArticlesType::class, $article, [
            'createAtShow' => true,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('articulos_articles_index');
        }

        return $this->render('articulos/articles/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="articulos_articles_show", methods={"GET"})
     */
    public function show(Articles $article): Response
    {
        return $this->render('articulos/articles/show.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="articulos_articles_edit", methods={"GET","POST"})
     * @isGranted("MANAGE", subject="article")
     */
    public function edit(Request $request, Articles $article): Response
    {
        $form = $this->createForm(ArticlesType::class, $article, [
            'createAtShow' => false,
        ]);

        // if ($article->getAuthor() != $this->getUser() && !$this->isGranted('ROLE_ADMIN_ARTICLE')) {
        //     throw $this->createAccessDeniedException('no Access');
        // }

        // $this->denyAccessUnlessGranted('MANAGE', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Aticle created knowledge is power!');
            //$this->get('session')->getFlashBag()->set('succes','Aticle created knowledge is power!');
            return $this->redirectToRoute('articulos_articles_index');
        }

        return $this->render('articulos/articles/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="articulos_articles_delete", methods={"DELETE"})
     * @isGranted("MANAGE", subject="article")
     */
    public function delete(Request $request, Articles $article): Response
    {
        $media = $this->getDoctrine()->getRepository(Multimedia::class)->findOneByIdArticulo($article->getId());
        $em = $this->getDoctrine()->getManager();
        foreach ($media as $entity) {
            $em->remove($entity);
        }
        $em->flush();

        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('articulos_articles_index');
    }
}
