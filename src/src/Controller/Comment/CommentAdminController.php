<?php

namespace App\Controller\Comment;

use App\Repository\Comment\CommentsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/comment")
 */
class CommentAdminController extends AbstractController
{
    /**
     * @Route("/", name="comment_admin")
     */
    public function index(CommentsRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        $q = $request->query->get('q');

        $queryBuilder = $repository->getWithShareQuerybuilder($q);

        $paginator = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            10 /*limit per page*/
        );

        return $this->render('comment_admin/comment.html.twig', [
            'pagination' => $paginator,
        ]);
    }
}
