<?php

namespace App\Controller;

use App\Entity\User\UserProfile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends AbstractController
{
    protected function getUser(): UserProfile
    {
        return parent::getUser();
    }
}
