<?php

namespace App\Controller\Home;

use App\Controller\BaseController;
use App\Entity\Articulos\Articles;
use App\Repository\Article\ArticlesRepository;
use App\Repository\Article\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/" )
 */
class HomeController extends BaseController
{
    /**
     * @Route("/" , name="index")
     */
    public function index(CategorieRepository $category)
    {
        $categorys = $category->findAll();
        // \Swift_Mailer $mailer;
        // $message =(new \Swift_Message('Hello Email'))
        //         ->setFrom('dannybombastic@gmail.com')
        //         ->setTo('dannybombastic@gmail.com')
        //         ->setBody('You should see me from the profiler!')
        //         ;
        //         $mailer->send($message);


        return $this->render('home/home.html.twig', [
            'categorys' => $categorys,
        ]);
    }

    /**
     * @Route("/articulos" , name="articulos")
     */
    public function articulos(ArticlesRepository $articles)
    {
        $articles = $articles->findNewestArticles();

        return $this->render('home/home.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/{id}/detail" , name="app_detail")
     */
    public function detail(Request $request, Articles $article)
    {
        if (!$article) {
            $this->redirectToRoute('index');
        }

        return $this->render('home/detail/detail.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/detail/{id}/heart" , name="app_detail_heart")
     */
    public function hintHeart(Request $request, Articles $article, EntityManagerInterface $em)
    {
        if (!$article) {
            $this->redirectToRoute('index');
        }

        $article->incrementHerts();
        $em->flush();

        return new JsonResponse([
            'heart' => $article->getHeartCount(),
        ]);
    }
}
