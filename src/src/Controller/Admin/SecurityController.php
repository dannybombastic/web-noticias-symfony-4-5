<?php

namespace App\Controller\Admin;

use App\Entity\User\UserProfile;
use App\Form\User\UserProfileType;
use App\Security\ServiceAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $userPassword, GuardAuthenticatorHandler $guardAuthenticate, ServiceAuthenticator $serviceAuthenticator)
    {
        $form = $this->createForm(UserProfileType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UserProfile $user */
            $user = $form->getData();

            $user->setPassword($userPassword->encodePassword(
                $user,
                $form['password']->getData()
            ));
            if (true === $form['agreeTerms']->getData()) {
                $user->agreeterms();
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $guardAuthenticate->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $serviceAuthenticator,
                'main'
            );
        }

        // if ($request->isMethod('POST')) {
        //     $user = new UserProfile();
        //     $user->setEmail($request->get('email'));
        //     $user->setUserName($request->get('username'));
        //     $user->setGender($request->get('gender'));
        //     $user->setId($request->get('id'));
        //     $user->setUserAddr($request->get('addr'));
        //     $user->setIsActive(1);

        //     $user->setRoles(['ROLE_USER']);

        //     $em = $this->getDoctrine()->getManager();
        //     $em->persist($user);
        //     $em->flush();
        // }
        return $this->render('security/register.html.twig', ['register_form' => $form->createView()]);
    }
}
