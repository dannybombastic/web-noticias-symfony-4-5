<?php

namespace App\Controller\Admin;

use App\Api\Articles\ArticleReferencesUploadApiModel;
use App\Entity\Articulos\Articles;
use App\Entity\Articulos\ArticlesReferences;
use App\Repository\User\UserProfileRepository;
use App\Service\UploaderService\UpLoadHelper;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File as FileFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminUtilityController extends AbstractController
{
    /**
     * @Route("/admin/utility", methods={"POST"}, name="admin_utility_users")
     * $IsGranted("ROLE_ADMIN")
     */
    public function getUserApi(UserProfileRepository $userRepository, Request $request)
    {
        $query = $request->request->get('query') ?? false;

        if (false == $query) {
            return $this->json([
                'message' => 'User Not Found',
            ], 404, [], ['groups' => ['main']]);
        }
        $users = $userRepository->findAllMatching($query);

        return $this->json([
            'users' => $users,
        ], 200, [], ['groups' => ['main']]);
    }

    /**
     * @Route("/admin/{id}/downloads", name="admin_utility_uploads", methods={"POST"})
     * @IsGranted("MANAGE", subject="article")
     */
    public function uploadUserApiPrivateFiles(Articles $article, Request $request, UpLoadHelper $uploaderHelper, EntityManagerInterface $em, ValidatorInterface $validator, SerializerInterface $serializer, ArticleReferencesUploadApiModel $uploadModel)
    {
        if ('application/json' === $request->headers->get('Content-Type')) {
            
            /** @var ArticleReferencesUploadApiModel $uploadModel */
            
            $uploadModel = $serializer->deserialize(
                $request->getContent(),
                ArticleReferencesUploadApiModel::class,
                'json'
            );

            $violations = $validator->validate($uploadModel);
            if ($violations->count() > 0) {
                return $this->json($violations, 400);
            }

            $tmpPath = sys_get_temp_dir() . '/sf_upload' . uniqid();

            file_put_contents($tmpPath, $uploadModel->getDecoderDate());
            $uploadFile = new FileFile($tmpPath);
            $originalFilename = $uploadModel->filename;
            

        } else {
            /** @var UploadedFile $uploadFile */
            $request->files->get('reference');
            $uploadFile = $request->files->get('reference');
            $originalFilename = $uploadFile->getClientOriginalName();

        }

        $violations = $validator->validate(
            $uploadFile,
            [
                new File([
                    'maxSize' => '5M',
                    'mimeTypes' => [
                        'image/*',
                        'application/pdf',
                        'application/msword',
                        'application/vnd.ms-excel',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                        'text/plain',
                        'text/html',
                    ],
                ]),
                new NotNull([
                    'message' => 'Not empty please mongolo',
                ]),
            ]
        );

        if ($violations->count() > 0) {
            return $this->json($violations, 200);
        }

        if ($article && $uploadFile) {
            $filename = $uploaderHelper->uploadArticleReference($uploadFile);
            $articleReference = new ArticlesReferences($article);
            $articleReference->setFilename($filename);
            $articleReference->setOriginalFilename($originalFilename ?? $filename);
            $articleReference->setMimeType($uploadFile->getMimeType(), 'application/octet-strem');
            
            if (is_file($uploadFile->getPathname())) {
                unlink($uploadFile->getPathname());
            }
    
            $em->persist($articleReference);
            $em->flush();

            return  $this->json($articleReference, 201, [], ['groups' => 'main']);
        }
    }

    /**
     * @Route("/admin/article/references/{id}/download", name="admin_utility_downloads", methods={"GET"})
     */
    public function downloadArticleReferences(ArticlesReferences $reference, UpLoadHelper $uploaderHelper, S3Client $s3Client, string $s3BucketName)
    {
        $article = $reference->getArticle();
        $this->denyAccessUnlessGranted('MANAGE', $article);

        // $response = new StreamedResponse(function () use ($reference, $uploaderHelper) {
        //     $outputStream = fopen('php://output', 'wb');
        //     $fileStrem = $uploaderHelper->readStream($reference->getFilePath(), false);
        //     stream_copy_to_stream($fileStrem, $outputStream);
        // });

        // $response->headers->set('Content-Type', $reference->getMimeType());
        // $disposition = HeaderUtils::makeDisposition(
        //     HeaderUtils::DISPOSITION_ATTACHMENT,
        //     $reference->getOriginalFilename()
        // );
        // $response->headers->set('Content-Disposition', $disposition);
        // return $response;

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $reference->getOriginalFilename()
        );

        $command = $s3Client->getCommand('GetObject', [
            'Bucket' => $s3BucketName,
            'Key' => $reference->getFilePath(),
            'ResposeContentType' => $reference->getMimeType(),
            'ResposeContentDisposition' => $disposition,
        ]);
        $request = $s3Client->createPresignedRequest($command, '+30 minutes');

        return new RedirectResponse((string) $request->getUri());
    }

    /**
     * @Route("/admin/article/{id}/references", name="admin_utility_downloads_list", methods={"GET"})
     * @IsGranted("MANAGE", subject="article")
     */
    public function getArticlereferences(Articles $article)
    {
        return $this->json($article->getarticlesReferences(), 200, [], ['groups' => 'main']);
    }

    /**
     * @Route("/admin/article/references/{id}", name="admin_utility_references_delete", methods={"DELETE"})
     */
    public function deleteArticleRefences(ArticlesReferences $reference, UpLoadHelper $uploaderHelper, EntityManagerInterface $em)
    {
        $article = $reference->getArticle();
        $this->denyAccessUnlessGranted('MANAGE', $article);
        $em->remove($reference);
        $em->flush();
        $uploaderHelper->deleteFile($reference->getFilePath(), false);

        return new Response(null, 204);
    }

    /**
     * @Route("/admin/article/references/{id}", name="admin_utility_references_update", methods={"PUT"})
     */
    public function updateArticleRefences(ArticlesReferences $reference, UpLoadHelper $uploaderHelper, Request $request, EntityManagerInterface $em, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $article = $reference->getArticle();

        $this->denyAccessUnlessGranted('MANAGE', $article);

        // populate entity to validate format inverse serializer
        $serializer->deserialize(
            $request->getContent(),
            ArticlesReferences::class,
            'json',
            [
                'object_to_populate' => $reference,
                'groups' => ['input', 'main'],
            ]
        );


        $violations = $validator->validate($reference);
        if ($violations->count() > 0) {
            return $this->json($violations, 400);
        }

        $em->persist($reference);
        $em->flush();

        return $this->json($reference, 200, [], ['groups' => 'main']);
    }

    /**
     * @Route("/admin/article/{id}/references/reorder", name="admin_utility_downloads_list_reorder", methods={"POST"})
     * @IsGranted("MANAGE", subject="article")
     */
    public function reorderArticleReferences(Articles $article, Request $request, EntityManagerInterface $em)
    {
        $orderedIds = json_decode($request->getContent(), true);

        if (false === $orderedIds) {
            return $this->json(['detail' => 'invalid body'], 400);
        }
        /**
         * give as an  associative array with keys  mapped by ids.
         */
        $orderedIds = array_flip($orderedIds);
        foreach ($article->getarticlesReferences() as $reference) {
            /* @var ArticlesReferences $reference */
            $reference->setPosition($orderedIds[$reference->getId()]);
        }
        $em->flush();

        return $this->json($article->getarticlesReferences(), 200, [], ['groups' => 'main']);
    }
}
