<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\User\UserProfile;
use App\Repository\User\UserProfileRepository;
use LogicException;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class EmailToUserTransformer implements DataTransformerInterface
{
    private $finderCall;
    private $userRepository;

    public function __construct(UserProfileRepository $userRepository, callable $finderCall)
    {
        $this->userRepository = $userRepository;
        $this->finderCall = $finderCall;
    }

    public function transform($value)
    {
        if (null === $value) {
            return '';
        }
        if (!$value instanceof UserProfile) {
            throw new LogicException('The UserSelecttextType can only be a user obj');
        }

        return $value->getEmail();
    }

    public function reverseTransform($value)
    {
        if (!$value) {
            return;
        }
        $callback = $this->finderCall;
        $user = $callback($this->userRepository, $value);

        //  $user = $this->userRepository->findOneBy(['Email' => $value]);
        if (!$user) {
            throw new TransformationFailedException(sprintf('No user found with email %s', $value));
        }

        return $user;
    }
}
