<?php

namespace App\Form\Articulos;

use App\Entity\Articulos\Categories;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotNull;

class CategoriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $category = $options['data'] ?? null;
        $isEdit = $category->getImage();
        $message = $isEdit;

        $builder
            ->add('name');

        $imageConstraints[] = new Image([
            'maxSize' => '1024k',
            'mimeTypes' => [
                'image/jpeg',
                'image/png',
                'image/jpeg',
            ],
            'mimeTypesMessage' => 'Please upload a valid Img Format',
        ]);

        if (null === $isEdit) {
            $imageConstraints[] = new NotNull([
                'message' => 'Please upload an image',
            ]);
            $message = 'Select an categorie image';
        }
        dump($isEdit);
        $builder
            ->add('image', FileType::class, [
                'attr' => [
                    'placeholder' => $message,
                ],
                'label' => 'imagen',
                'required' => false,
                'mapped' => false,
                'constraints' => $imageConstraints,
            ])
            ->add('isActive');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Categories::class,
        ]);
    }
}
