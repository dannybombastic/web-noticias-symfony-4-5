<?php

namespace App\Form\Articulos;

use App\Entity\Articulos\Articles;
use App\Entity\Articulos\Categories;
use App\Entity\User\UserProfile;
use App\Form\User\UserSelectTextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticlesType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Articles::class,
            'createAtShow' => false,
            'empty_data' => date('Y-m-d H:i:s'),
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $article = $options['data'] ?? null;
        $isEdit = $article && $article->getId();

        $builder
            ->add('artTitle', TextType::class, [
                'attr' => [
                    'placeholder' => 'Introduzca un Titule',
                    'aria-describedby' => 'emailHelp',
                    'class' => 'form-control',
                ],
                'label' => 'Articulo',
            ])
            ->add('artDesc', TextareaType::class, [
                'rows' => 10,
                'attr' => [
                    'placeholder' => 'Introduzca la descripcion',
                    'aria-describedby' => 'emailHelp',
                    'class' => 'form-control',
                ],
                'label' => 'Decsripcion Articulo',
            ])
            ->add('artEvalu', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Introduzca la evaluacion',
                    'aria-describedby' => 'emailHelp',
                    'class' => 'form-control',
                ],
                'label' => 'Evaluacion',
            ])
            ->add('visitedCnt', NumberType::class, [
                'attr' => [
                    'placeholder' => 'Introduzca la evaluacion',
                    'aria-describedby' => 'emailHelp',
                    'class' => 'form-control',
                ],
                'label' => 'Visitas Articulo',
            ])

          ->add(
              'idCat',
              EntityType::class,
              [
                    'class' => Categories::class,
                    'choice_label' => function (Categories $categorie) {
                        return $categorie->getName();
                    },
                    'label' => 'CATEGORIA',
                    'attr' => [
                        'class' => 'form-control',
                    ],
                    'choice_attr' => function ($choice, $key, $value) {
                        // adds a class like attending_yes, attending_no, etc
                        return ['class' => 'attending_'.strtolower($key)];
                    },
                ]
          )->add('author', UserSelectTextType::class, [
                'disabled' => $isEdit,
                // 'class' => UserProfile::class,
                // 'choice_label' => function(UserProfile $user){
                //     return $user->getUserName();
                // },
                // 'placeholder' => 'Choose an Author',
                // 'invalid_message' => 'I am to smart for your hacking!'
                //'choice_label' => 'email'
            ]);

        if ($options['createAtShow']) {
            $builder->add('visitedAt', DateTimeType::class, [
                'label' => 'Plubished date',
                'input' => 'datetime',
                'data' => new \DateTime('now'),
                'time_label' => 'Starts On',
            ]);
        }
    }
}
