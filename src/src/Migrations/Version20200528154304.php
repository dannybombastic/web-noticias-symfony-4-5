<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200528154304 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE api_token_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tageed_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE articles_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE articles_references_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tags_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_profile (id VARCHAR(24) NOT NULL, user_name VARCHAR(240) NOT NULL, user_addr VARCHAR(60) NOT NULL, user_email VARCHAR(240) NOT NULL, user_password VARCHAR(150) NOT NULL, gender VARCHAR(255) NOT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_active BOOLEAN NOT NULL, roles TEXT NOT NULL, aggre_terms_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN user_profile.roles IS \'(DC2Type:simple_array)\'');
        $this->addSql('CREATE TABLE api_token (id INT NOT NULL, user_id VARCHAR(24) NOT NULL, token VARCHAR(255) NOT NULL, exprite_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7BA2F5EBA76ED395 ON api_token (user_id)');
        $this->addSql('CREATE TABLE user_roles (id SERIAL NOT NULL, user_id VARCHAR(24) NOT NULL, roles TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_54FCD59FA76ED395 ON user_roles (user_id)');
        $this->addSql('COMMENT ON COLUMN user_roles.roles IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE tageed (id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(180) NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3F9B4C5B989D9B62 ON tageed (slug)');
        $this->addSql('CREATE TABLE articles (id INT NOT NULL, id_cat UUID DEFAULT NULL, author_id VARCHAR(24) NOT NULL, art_title VARCHAR(220) NOT NULL, art_desc VARCHAR(400) NOT NULL, art_evalu INT NOT NULL, visited_cnt INT NOT NULL, visited_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_active BOOLEAN NOT NULL, heart_count INT DEFAULT NULL, point geometry(POINT, 4326) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BFDD3168FAABF2 ON articles (id_cat)');
        $this->addSql('CREATE INDEX IDX_BFDD3168F675F31B ON articles (author_id)');
        $this->addSql('COMMENT ON COLUMN articles.id_cat IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE articles_tageed (articles_id INT NOT NULL, tageed_id INT NOT NULL, PRIMARY KEY(articles_id, tageed_id))');
        $this->addSql('CREATE INDEX IDX_85A398761EBAF6CC ON articles_tageed (articles_id)');
        $this->addSql('CREATE INDEX IDX_85A398763DB94E9B ON articles_tageed (tageed_id)');
        $this->addSql('CREATE TABLE categories (id UUID NOT NULL, name VARCHAR(220) NOT NULL, image VARCHAR(220) NOT NULL, is_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN categories.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE articles_references (id INT NOT NULL, article_id INT NOT NULL, filename VARCHAR(255) NOT NULL, original_filename VARCHAR(255) NOT NULL, mime_type VARCHAR(255) NOT NULL, position INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7BED21FA7294869C ON articles_references (article_id)');
        $this->addSql('CREATE TABLE tags (id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE comments (id SERIAL NOT NULL, id_user VARCHAR(24) DEFAULT NULL, article_id INT NOT NULL, art_comment TEXT NOT NULL, is_active BOOLEAN DEFAULT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, authorname VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5F9E962A6B3CA4B ON comments (id_user)');
        $this->addSql('CREATE INDEX IDX_5F9E962A7294869C ON comments (article_id)');
        $this->addSql('CREATE TABLE menu (id SERIAL NOT NULL, id_menu INT NOT NULL, href VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE api_token ADD CONSTRAINT FK_7BA2F5EBA76ED395 FOREIGN KEY (user_id) REFERENCES user_profile (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_roles ADD CONSTRAINT FK_54FCD59FA76ED395 FOREIGN KEY (user_id) REFERENCES user_profile (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE articles ADD CONSTRAINT FK_BFDD3168FAABF2 FOREIGN KEY (id_cat) REFERENCES categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE articles ADD CONSTRAINT FK_BFDD3168F675F31B FOREIGN KEY (author_id) REFERENCES user_profile (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE articles_tageed ADD CONSTRAINT FK_85A398761EBAF6CC FOREIGN KEY (articles_id) REFERENCES articles (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE articles_tageed ADD CONSTRAINT FK_85A398763DB94E9B FOREIGN KEY (tageed_id) REFERENCES tageed (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE articles_references ADD CONSTRAINT FK_7BED21FA7294869C FOREIGN KEY (article_id) REFERENCES articles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A6B3CA4B FOREIGN KEY (id_user) REFERENCES user_profile (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A7294869C FOREIGN KEY (article_id) REFERENCES articles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE api_token DROP CONSTRAINT FK_7BA2F5EBA76ED395');
        $this->addSql('ALTER TABLE user_roles DROP CONSTRAINT FK_54FCD59FA76ED395');
        $this->addSql('ALTER TABLE articles DROP CONSTRAINT FK_BFDD3168F675F31B');
        $this->addSql('ALTER TABLE comments DROP CONSTRAINT FK_5F9E962A6B3CA4B');
        $this->addSql('ALTER TABLE articles_tageed DROP CONSTRAINT FK_85A398763DB94E9B');
        $this->addSql('ALTER TABLE articles_tageed DROP CONSTRAINT FK_85A398761EBAF6CC');
        $this->addSql('ALTER TABLE articles_references DROP CONSTRAINT FK_7BED21FA7294869C');
        $this->addSql('ALTER TABLE comments DROP CONSTRAINT FK_5F9E962A7294869C');
        $this->addSql('ALTER TABLE articles DROP CONSTRAINT FK_BFDD3168FAABF2');
        $this->addSql('DROP SEQUENCE api_token_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tageed_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE articles_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE articles_references_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tags_id_seq CASCADE');

        $this->addSql('DROP TABLE user_profile');
        $this->addSql('DROP TABLE api_token');
        $this->addSql('DROP TABLE user_roles');
        $this->addSql('DROP TABLE tageed');
        $this->addSql('DROP TABLE articles');
        $this->addSql('DROP TABLE articles_tageed');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE articles_references');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE comments');
        $this->addSql('DROP TABLE menu');
    }
}
