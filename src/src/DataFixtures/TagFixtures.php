<?php

namespace App\DataFixtures;

use App\Entity\Tags\Tageed;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Tageed::class, 20, function (Tageed $tags, $count) {
            $tags->setName($this->faker->realText(20));
        });
        $manager->flush();
    }
}
