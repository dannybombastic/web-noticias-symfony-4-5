<?php

namespace App\DataFixtures;

use App\Entity\User\ApiToken;
use App\Entity\User\UserProfile;
use DateTime;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends BaseFixture
{
    public function loadData(ObjectManager $manager)
    {
        $admin = new UserProfile();
        $admin->setId('79020208m');
        $admin->setUserName('Daniel Urbano');
        $admin->setUserAddr('Falsa direccion');
        $admin->setuserEmail('dannybombastic@gmail.com');
        $admin->setGender('M');
        $admin->setCreateAt(new DateTime());
        $admin->agreeterms();
        $admin->setIsActive(1);
        $role = $admin->getRoles();

        if (!in_array('ROLE_ADMIN', $role, true)) {
            array_push($role, 'ROLE_ADMIN');
            $admin->setRoles($role);
        }
        $admin->setPassword($this->encoder->encodePassword(
            $admin,
            '659011563'
        ));

        $token = new ApiToken($admin);
        $manager->persist($token);
        $manager->persist($admin);
        
        $manager->flush();

        $this->createMany(UserProfile::class, 20, function (UserProfile $users, $count) {
            $users->setId('797455'.$count);
            $users->setUserName($this->faker->name);
            $users->setUserAddr($this->faker->streetAddress);
            $users->setuserEmail($this->faker->email);
            $users->setGender('M');

            $users->setCreateAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $users->agreeterms();
            $users->setIsActive(1);
            $users->setPassword($this->encoder->encodePassword(
                $users,
                '659011563'
            ));
        });

        $manager->flush();

        // $users = new UserProfile();
        // $users->setId("797455$i");
        // $users->setUserName('dannybombastic');
        // $users->setUserAddr('dannybombastic');
        // $users->setuserEmail('dannybombastic@gmail.com');
        // $users->setGender('M');
        // $users->agreeterms();
        // $users->setRoles(['ROLE_ADMIN_EDITOR', 'ROLE_ADMIN']);
        // $users->setCreateAt(New DateTime());
        // $users->setIsActive(1);
        // $users->setPassword($this->userPassword->encodePassword(
        //     $users,
        //      '659011563'
        // ));
        // $apitoken = new ApiToken($users);
        // $manager->persist($apitoken);
        // $manager->persist($users);
        // $manager->flush();
    }
}
