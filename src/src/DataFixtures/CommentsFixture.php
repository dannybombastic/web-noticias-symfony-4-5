<?php

namespace App\DataFixtures;

use App\Entity\Articulos\Articles;
use App\Entity\Comment\Comments;
use App\Entity\User\UserProfile;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentsFixture extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Comments::class, 150, function (Comments $comments, $count) {
            /** @var UserProfile $user */
            $user = $this->getReference(UserProfile::class.'_'.$this->faker->numberBetween(0, 19));
            $comments->setIdUser($user);
            $comments->setArticle($this->getReference(Articles::class.'_'.$this->faker->numberBetween(0, 19)));
            $comments->setArtComment($this->faker->paragraph($nbSentences = 3, $variableNbSentences = true));
            $comments->setCreateAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $comments->setIsActive($this->faker->boolean(20));
            $comments->setAuthorname($user->getUserName());
        });
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ArticlesFixtures::class,
        ];
    }
}
