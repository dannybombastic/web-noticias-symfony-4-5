<?php

namespace App\DataFixtures;

use App\Entity\Articulos\Articles;
use App\Entity\Articulos\Categories;
use App\Entity\Tags\Tageed;
use App\Entity\User\UserProfile;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ArticlesFixtures extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Articles::class, 20, function (Articles $article, $count) {
            // create 20 products! Bam!
            $article->setArtTitle($this->faker->sentence($nbWords = 6, $variableNbWords = true));
            $article->setArtEvalu($this->faker->numberBetween(5, 10));
            $article->setArtDesc($this->faker->realText($maxNbChars = 200, $indexSize = 2));
            $article->setVisitedCnt($this->faker->numberBetween(0, 15));
            $article->setVisitedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $article->setCreateAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $article->setPoint('SRID=4326;POINT('.(self::LOACATIONS[$this->faker->numberBetween(0, 99)])[1].' '.(self::LOACATIONS[$this->faker->numberBetween(0, 99)])[0].')');
            //$article->setPoint('SRID=4326;POINT(-4.891400 36.508300)');
            //$article->setPoint('SRID=3785;POINT(36.715112 -4.423331)');

            $article->setIsActive(true);
            $article->setIdCat($this->getReference(Categories::class.'_'.$this->faker->numberBetween(0, 19)));
            $article->setAuthor($this->getReference(UserProfile::class.'_'.$this->faker->numberBetween(0, 19)));
            /** @var Tageed[] $tags */
            $tags = $this->getRandomReferences(Tageed::class, $this->faker->numberBetween(0, 5));
            foreach ($tags as $tag) {
                $article->addTag($tag);
            }
        });
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
