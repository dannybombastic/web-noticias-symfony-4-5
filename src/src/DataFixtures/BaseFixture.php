<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User\ApiToken;
use App\Entity\User\UserProfile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\Lorem;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

abstract class BaseFixture extends Fixture
{
    /** @var ObjectManager */
    private $manager;

    /** @var Lorem */
    protected $faker;
    /** @var UserPasswordEncoderInterface */
    protected $encoder = [];

    private $referenceIndex;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    abstract protected function loadData(ObjectManager $em);

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->loadData($manager);
    }

    protected function createMany(string $className, int $count, callable $factory)
    {
        for ($i = 0; $i < $count; ++$i) {
            if (ApiToken::class === $className) {
                $entity = new $className($this->getReference(UserProfile::class.'_'.$this->faker->numberBetween(0, 19)));
            } else {
                $entity = new $className();
            }

            $factory($entity, $i);

            $this->manager->persist($entity);
            // store for usage later as App\Entity\ClassName_#COUNT#
            $this->addReference($className.'_'.$i, $entity);
        }
    }

    protected function getRandomReference(string $className)
    {
        if (!isset($this->referencesIndex[$className])) {
            $this->referencesIndex[$className] = [];

            foreach ($this->referenceRepository->getReferences() as $key => $ref) {
                if (0 === strpos($key, $className.'_')) {
                    $this->referencesIndex[$className][] = $key;
                }
            }
        }

        if (empty($this->referencesIndex[$className])) {
            throw new \Exception(sprintf('Cannot find any references for class "%s"', $className));
        }

        $randomReferenceKey = $this->faker->randomElement($this->referencesIndex[$className]);

        return $this->getReference($randomReferenceKey);
    }

    protected function getRandomReferences(string $className, int $count)
    {
        $references = [];
        while (count($references) < $count) {
            $references[] = $this->getRandomReference($className);
        }

        return $references;
    }

    const LOACATIONS = [
        ['36.715112', '-4.423331'],
        ['36.731746', '-4.381694'],
        ['36.715276', '-4.423396'],
        ['36.713408', '-4.430337'],
        ['36.719249', '-4.407877'],
        ['36.492917', '-4.950179'],
        ['36.724191', ' -4.41898'],
        ['36.727695', '-4.412187'],
        ['36.727695', '-4.412187'],
        ['36.727695', '-4.412187'],
        ['36.727769', '-4.412079'],
        ['36.718413', '-4.411814'],
        ['36.718413', '-4.411814'],
        ['36.714169', '-4.428849'],
        ['36.729482', ' -4.41429'],
        ['36.731746', '-4.381694'],
        ['36.714575', '-4.309726'],
        ['36.715459', ' -4.42734'],
        ['36.718414', '-4.411823'],
        ['36.724163', '-4.392713'],
        ['36.717273', '-4.411371'],
        ['36.720975', ' -4.34933'],
        ['36.724533', '-4.419406'],
        ['36.720496', '-4.404485'],
        ['36.723328', '-4.418669'],
        ['36.727264', '-4.422909'],
        ['36.726256', '-4.397079'],
        ['36.727695', '-4.412187'],
        ['36.727769', '-4.412079'],
        ['36.723746', '-4.417241'],
        ['36.724048', '-4.418311'],
        ['36.71969', '-4.344865'],
        ['36.726424', '-4.380052'],
        ['36.726424', '-4.380052'],
        ['36.71353', '-4.432092'],
        ['36.717273', '-4.411371'],
        ['36.720561', '-4.422586'],
        ['36.726062', '-4.415433'],
        ['36.693255', '-4.443059'],
        ['36.492917', '-4.950179'],
        ['36.502422', '-4.712758'],
        ['36.732859', '-4.416906'],
        ['36.502422', '-4.712758'],
        ['36.488482', '-4.712342'],
        ['36.581812', '-4.538879'],
        ['36.724239', '-4.418922'],
        ['36.722454', '-4.421277'],
        ['36.61405', ' -4.50135'],
        ['36.71108', ' -4.42736'],
        ['36.692299', '-4.441252'],
        ['36.719664', '-4.421192'],
        ['36.719124', '-4.423592'],
        ['36.719234', '-4.424014'],
        ['36.716542', '-4.423154'],
        ['36.716376', '-4.421216'],
        ['36.714233', '-4.423397'],
        ['36.716268', '-4.422084'],
        ['36.71726', '-4.422285'],
        ['36.716314', '-4.422161'],
        ['36.716314', '-4.422161'],
        ['36.718404', ' -4.41177'],
        ['36.727062', '-4.420296'],
        ['36.725976', '-4.419751'],
        ['36.720883', '-4.420023'],
        ['36.722739', '-4.419571'],
        ['36.718069', '-4.422249'],
        ['36.718069', '-4.422249'],
        ['36.720527', '-4.417697'],
        ['36.720526', ' -4.42253'],
        ['36.724074', '-4.415915'],
        ['36.723938', '-4.416203'],
        ['36.725354', '-4.417921'],
        ['36.728011', '-4.412903'],
        ['36.725405', '-4.419005'],
        ['36.725269', '-4.419148'],
        ['36.711204', '-4.426523'],
        ['36.719363', '-4.424192'],
        ['36.725131', '-4.427818'],
        ['36.725131', '-4.427818'],
        ['36.720151', '-4.409682'],
        ['36.717316', '-4.411281'],
        ['36.717316', '-4.411281'],
        ['36.717886', '-4.410877'],
        ['36.596103', ' -4.54324'],
        ['36.596103', ' -4.54324'],
        ['36.723213', '-4.418496'],
        ['36.723213', '-4.418496'],
        ['36.723357', '-4.418669'],
        ['36.606777', '-4.511973'],
        ['36.721711', '-4.375543'],
        ['36.334343', '-5.242411'],
        ['36.593794', '-4.525485'],
        ['36.721186', '-4.376053'],
        ['36.721191', '-4.376064'],
        ['36.721699', '-4.375564'],
        ['36.712575', '-4.430103'],
        ['36.712567', '-4.430096'],
        ['36.639702', '-4.491376'],
        ['36.639704', '-4.491376'],
        ['36.543257', '-4.621409'],
    ];
}
