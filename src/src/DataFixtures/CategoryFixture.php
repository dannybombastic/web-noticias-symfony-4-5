<?php

namespace App\DataFixtures;

use App\Entity\Articulos\Categories;
use App\Service\UploaderService\UpLoadHelper;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class CategoryFixture extends BaseFixture
{
    private UpLoadHelper $upLoadHelper;

    public function __construct(UpLoadHelper $upLoadHelper)
    {
        $this->upLoadHelper = $upLoadHelper;
    }

    protected function loadData(ObjectManager $manager)
    {
        $fotos = [
            'arte.jpeg',
            'galaxy.jpg',
        ];

        $this->createMany(Categories::class, 20, function (Categories $category, $count) use ($fotos) {
            $fs = new Filesystem();
            $foto = $fotos[$this->faker->numberBetween(0, 1)];
            $targetPath = sys_get_temp_dir().$foto;
            $fs->copy(__DIR__.'/images/'.$foto, $targetPath, true);

            $imageFilename = $this->upLoadHelper
                ->uploadCategorieImage(new File($targetPath), null);

            $category->setName($this->faker->name);
            $category->setImage($imageFilename);
            $category->setIsActive(true);
        });

        $manager->flush();
    }
}
