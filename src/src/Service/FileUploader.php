<?php

declare(strict_types=1);

namespace App\Service;

use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    const CATEGORY_IMAGE_FLDR = 'categories';
    private string $targetDirectory;
    private RequestStackContext $requestContext;

    public function __construct(string $targetDirectory, RequestStackContext $requestContext)
    {
        $this->targetDirectory = $targetDirectory.'/'.self::CATEGORY_IMAGE_FLDR;
        $this->requestContext = $requestContext;
    }

    public function upload(UploadedFile $file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $fileName = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    public function getPublicPath(string $path): string
    {
        return $this->requestContext
            ->getBasePath().'build/uploads/'.$path;
    }

    public function targerFolder(string $var = '')
    {
    }
}
