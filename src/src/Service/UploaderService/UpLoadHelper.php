<?php

declare(strict_types=1);

namespace App\Service\UploaderService;

use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\AdapterInterface;
use League\Flysystem\Exception;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UpLoadHelper
{
    const CATEGORY_IMAGE = 'categories';
    const ARTICLE_REFERENCES = 'categories';
    private FilesystemInterface $uploadsFilesystem;
    private RequestStackContext $requestStack;
    private LoggerInterface $logger;
    private string $uploadedAssetsBaseUrl;
    private FilesystemInterface $privateUploadsFilesystem;

    public function __construct(FilesystemInterface $uploadsFilesystem, FilesystemInterface $privateUploadsFilesystem, RequestStackContext $requestStack, LoggerInterface $logger, string $uploadedAssetsBaseUrl)
    {
        $this->logger = $logger;
        $this->uploadsFilesystem = $uploadsFilesystem;
        $this->requestStack = $requestStack;
        $this->uploadedAssetsBaseUrl = $uploadedAssetsBaseUrl;
        $this->privateUploadsFilesystem = $privateUploadsFilesystem;
    }

    public function uploadCategorieImage(File $file, ?string $existingFilename): string
    {
        $newFilename = $this->uploadFile($file, self::ARTICLE_REFERENCES, true);

        if ($existingFilename) {
            try {
                $result = $this->uploadsFilesystem->delete('/'.$existingFilename);
                if (false === $result) {
                    throw new Exception(sprintf('Could not delete uploaded file "%s" ', $existingFilename));
                }
            } catch (FileNotFoundException $e) {
                $this->logger->alert(sprintf('File with this name "%s" does Not exists', $existingFilename));
            }
        }

        return $newFilename;
    }

    /**
     *  @return resource
     */
    public function readStream(string $path, bool $isPublic)
    {
        $filesystem = $isPublic ? $this->uploadsFilesystem : $this->privateUploadsFilesystem;

        $resource = $filesystem->readStream($path);
        if (false === $resource) {
            throw new Exception(sprintf('Error opening stream for  file "%s" ', $path));
        }

        return $resource;
    }

    public function uploadArticleReference(File $file)
    {
        return $this->uploadFile($file, self::ARTICLE_REFERENCES, false);
    }

    public function uploadFile(File $file, string $directory, bool $isPublic): string
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }

        $newFilename = Urlizer::urlize(pathinfo($originalFilename, PATHINFO_FILENAME)).'-'.uniqid().'.'.$file->guessExtension();

        $fylesystem = $isPublic ? $this->uploadsFilesystem : $this->privateUploadsFilesystem;

        $stream = fopen($file->getPathname(), 'r');

        $result = $fylesystem->writeStream(
            $directory.'/'.$newFilename,
            $stream,
            [
                'visibility' => $isPublic ? AdapterInterface::VISIBILITY_PUBLIC : AdapterInterface::VISIBILITY_PRIVATE,
            ]
        );

        if (false === $result) {
            throw new Exception(sprintf('Could not write uploaded file "%s" ', $newFilename));
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $newFilename;
    }

    public function getPublicPath(string $path): string
    {
        $fullPath = $this->uploadedAssetsBaseUrl.'/'.$path;
        if (strpos($fullPath, '://' !== false)) {
            return $fullPath;
        }

        return $this->requestStack
            ->getBasePath().$fullPath;
    }

    public function deleteFile(string $path, bool $isPublic)
    {
        $filesystem = $isPublic ? $this->uploadsFilesystem : $this->privateUploadsFilesystem;
        $result = $filesystem->delete($path);
        if (false === $result) {
            throw new Exception(sprintf('Could not write dlete file "%s" ', $path));
        }
    }
}
