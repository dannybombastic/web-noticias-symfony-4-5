import Dropzone from 'dropzone';
import 'dropzone/dist/dropzone.css';
import Sortable from 'sortablejs';
import "../css/autoc_article.scss";

Dropzone.autoDiscover = false;

(async () => {
    const module = await (
        await import('./components/autocomplete')).default;
    const $auto = $('.js-user-autocmplete');
    if (!$auto.is(':disabled')) {
        module($auto, 'users', 'Email');
    }

    var referenceList = new ReferenceList($('.js-reference-list'));
    initializeDropzone(referenceList)


})();




function initializeDropzone(referenceObj) {
    var formElement = document.querySelector('.js-reference-dropzone');
    if (!formElement) {
        return;
    }

    var dropzone = new Dropzone(formElement, {
        paramName: 'reference',
        init: function () {
            this.on('success', function (file, data) {
                referenceObj.addReferences(data);
            });
            this.on('error', function (file, data) {
                if (data.detail) {
                    this.emit('error', file, data.detail);
                }

            });
        },

    });

}


class ReferenceList {
    constructor($element) {
        this.$element = $element;
        this.references = [];
        this.Sortable = Sortable.create(this.$element[0], {
            handle: '.drag-handle',
            animation: 150,
            onEnd: () => {
                $.ajax({
                    type: "POST",
                    url: this.$element.data('url')+'/reorder',
                    data: JSON.stringify(this.Sortable.toArray()),
                    dataType: "json",
                    success: function (response) {

                    },
                    error: function (response) {

                    }
                });
            }
        });
        this.render();

        this.$element.on('click', '.js-reference-delete', (event) => {

            this.handleReferencedelete(event);
        });
        this.$element.on('blur', '.js-edit-filename', (event) => {
            this.handleReferenceEdit(event);
        });

        $.ajax({
            url: this.$element.data('url')
        }).then(data => {
            this.references = data;
            this.render();
        })
    }



    handleReferenceEdit(event) {
        const $li = $(event.currentTarget).closest('.list-group-item');
        const id = $li.data('id');
        const reference = this.references.find(reference => {
            return reference.id === id;
        });

        reference.originalFilename = $(event.currentTarget).val();

        $.ajax({
            type: "PUT",
            url: "/admin/article/references/" + id,
            data: JSON.stringify(reference),
            success: function (data) {
                console.log("succes", data)
            },
            error: function (data) {
                $('.alert.alert-danger').text(data.responseJSON.detail);
                $('#upload-error').show();
                $('.alert .close').on('click', function (e) {
                    $(this).parent().hide();
                });
                console.log("fail", data.responseJSON.detail)
            }
        });

    }



    handleReferencedelete(event) {
        const $li = $(event.currentTarget).closest('.list-group-item');
        const id = $li.data('id');

        $li.addClass('d-none');

        $.ajax({
            url: "/admin/article/references/" + id,
            method: "DELETE"
        }).then(() => {

            this.references = this.references.filter(reference => {
                return reference.id !== id;
            });


            this.render();
        });


    }
    addReferences(reference) {
        this.references.push(reference);
        this.render();

    }

    render() {
        const itemsHtml = this.references.map(reference => {
            return `<li class="list-group-item" data-id="${reference.id}">
          
            <div class="row">

            <div class="col-sm-8 text-left">
                <div class="d-flex justify-content-start">
                <div class="col-sm-2 p-2">
                    <span class="drag-handle fa fa-reorder"></span>
                    </div>
                    <div class="d-flex w-100 justify-content-end">
                        <input type="text" class="form-control js-edit-filename" id="input-update-${reference.id}"
                            value="${reference.originalFilename}" />
                    </div>
                </div>
            </div>
            <div class="col-sm-2 p-2">
                <a class="btn btn-link btn-sm text-dark" href="/admin/article/references/${reference.id}/download">
                    <i class="fa fa-download" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-sm-2 p-2">
                <button class="js-reference-delete btn btn-sm btn-link"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </div>
        </div>
            
        </li>`;
        });
        this.$element.html(itemsHtml.join(''));
    }
}
