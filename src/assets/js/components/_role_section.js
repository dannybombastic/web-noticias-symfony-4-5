import $ from 'jquery';


  
$('.add-another-collection-widget').click(function (e) {
  
    
    var list = $($(this).attr('data-list-selector'));
    // Try to find the counter of the list or use the length of the list
    var counter = list.data('widget-counter') || list.children().length;
    
    // grab the prototype template
    var newWidget = list.attr('data-prototype');
    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"
    counter++;
    console.log("prototype", newWidget);
    newWidget = newWidget.replace(/__name__/g, counter);
    // Increase the counter
   
  
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data('widget-counter', counter);

    
    // create a new list element and add it to the list
    var newElem = $(list.attr('data-widget-tags')).html(newWidget);
    newElem.appendTo(list);
    addTagFormDeleteLink(newElem);

});

deleteField();


function addTagFormDeleteLink($tagFormLi) {
    var $removeFormButton = $(`
    
        <div class="row  justify-content-end">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                <button class="btn btn-outline-danger" type="button">Delete this Role</button>
            </div>
        </div>

    `);
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function (e) {
        // remove the li for the tag form
        $tagFormLi.remove();
    });
}


function deleteField(){
    var elelments = $('li.role-delete-element');
    $.each(elelments, function (indexInArray, valueOfElement) { 
        if (indexInArray != 0) {
         addTagFormDeleteLink($(valueOfElement));
        }
    });
}


console.log("hola desde roles", "jodidosss");
