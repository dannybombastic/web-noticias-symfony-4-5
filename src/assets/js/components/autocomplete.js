import $ from 'jquery';

/** variable global yo ucan use it in the hold app  only legacy code support*/
// global.$ = $;

import 'bootstrap';
import autocomplete from 'autocomplete.js/dist/autocomplete.jquery.js';


export default function ($element, dataKey, displayKey) {

    $element.each(function () {

        var autocomplete = $(this).data('autocomplete-url');

        $(this).autocomplete({ hint: false }, [
            {
                source: function (query, cb) {
                    $.ajax({
                        method:'POST',
                        url: autocomplete,
                        data: {query: query}
                    }).then(function (data) {
                        if (dataKey) {
                            data = data[dataKey];
                        }
                        cb(data)
                    });
                },
                displayKey: displayKey,
                debounce: 250
            }
        ]);
    });
}