import "../../css/role_section.scss";   

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';


$('.custom-file-input').on('change', function (event) {
    var inputFile = event.currentTarget;


    // $(inputFile).parent()
    //         .find('.custom-file-label')
    //         .html(inputFile.files[0].name);

    inputFile.files.forEach(file => {
        $(inputFile).parent().parent()
            .find('.custom-file-label')
            .html(file.name);
    });
});
