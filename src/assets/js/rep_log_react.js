import React from 'react';
import { render } from 'react-dom';
import RepLogApp from './Replog/RepLogApp';


const shouldShowText = true;

render(
    <div>
        <RepLogApp with_heart={shouldShowText} />
    </div>,
    document.getElementById('root')

);