import React, { Component } from 'react';


export default class RepLogApp extends Component {
    constructor(props) {
        super();

        this.state = {
            highlight: null
        };
    }

    handlerClickRow(repLog, event){
        this.setState({highlight: repLog.id});
    }

    render() {

        const { highlight } = this.state;
        const { with_heart } = this.props;

        let heart = '';
        if (with_heart) {
            heart = <span >8====D</span>;
        }
      

        const repLogs = [
            { id: 1, reps: 25, itemLabel: 'My Laptop', totalWeightLifted: 112.5 },
            { id: 2, reps: 10, itemLabel: 'Big Fat Cat', totalWeightLifted: 180 },
            { id: 8, reps: 4, itemLabel: 'Big Fat Cat', totalWeightLifted: 72 }
        ];



        return (
            <div className="row">
                <div className="col-12">
                    <table className="table table-striped">
                        <thead>
                            <tr>

                                <th>What</th>
                                <th>How many times?</th>
                                <th>Weight</th>

                            </tr>
                        </thead>
                        <tbody>
                            {repLogs.map((repLog) => (
                                <tr
                                    key={repLog.id}
                                    className={highlight === repLog.id ? 'bg-info' : ''}
                                    onClick={ (event) => this.handlerClickRow(repLog, event)}
                                >

                                    <td>{repLog.itemLabel}</td>
                                    <td>{repLog.reps}</td>
                                    <td>{repLog.totalWeightLifted}</td>
                                    <td>...</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}


console.log(<RepLogApp />);
